package ru.niit.mygraphtest;
import java.awt.*;
import javax.swing.*;
public class MainClass {
	public static void main(String[] args) {
		JFrame jf = new JFrame("new JFrame");
		jf.setBounds(0, 0, 600, 600);
		Container cont = jf.getContentPane();
		
		JRadioButton jrb = new JRadioButton("1", true); // sozdayu radiobutton
		cont.add(jrb);                      // dobavliau radiobutton
		cont.add(new MyComponent());		// dobavliau liniu

		
		jf.setVisible(true);
	}
}

class MyComponent extends JComponent{
	private static final long serialVersionUID = 1L;
	public void paint(Graphics g){
		g.setColor(Color.red);
		g.drawLine(50, 50, 100, 100);
	}	
}